/*
 * wm87331.h
 *
 *  Created on: 2016
 *      Author: Grupo 5
 */

#include "board.h"
#include "chip.h"

#include <stdint.h>

// Registros del Codec WM8731
//#define WM8731_ADDRESS          	0x34       // WM8731 chip address on I2C bus
#define WM8731_ADDRESS          	0x1A       // WM8731 chip address on I2C bus
#define WM8731_REG_LLINE_IN      	0x00       // Left Channel Line Input Volume Control
#define WM8731_REG_RLINE_IN      	0x01       // Right Channel Line Input Volume Control
#define WM8731_REG_LHPHONE_OUT     	0x02       // Left Channel Headphone Output Volume Control
#define WM8731_REG_RHPHONE_OUT      0x03       // Right Channel Headphone Output Volume Control
#define WM8731_REG_ANALOG_PATH      0x04       // Analog Audio Path Control
#define WM8731_REG_DIGITAL_PATH     0x05       // Digital Audio Path Control
#define WM8731_REG_PDOWN_CTRL       0x06       // Power Down Control Register
#define WM8731_REG_DIGITAL_IF       0x07       // Digital Audio Interface Format
#define WM8731_REG_SAMPLING_CTRL    0x08       // Sampling Control Register
#define WM8731_REG_ACTIVE_CTRL      0x09       // Active Control
#define WM8731_REG_RESET            0x0F       // Reset register

// Estado inicial de los registros
#define _WM8731_left_lineIn         0x0180     // Mic settings: Enable mute, Enable simultaneous load to left and right channels
#define _WM8731_Right_lineIn        0x0180     // Mic settings: Enable mute, Enable simultaneous load to left and right channels
#define _WM8731_Left_hp             0x01F0     // Headphone settings : -9dB output
#define _WM8731_Right_hp            0x01F0     // Headphone settings : -9dB output
#define _WM8731_AnalogAudio         0xE4       // DAC Select
#define _WM8731_DigitalAudio        0x06	   // No deberia ser 7? => estamos activando HPF
#define _WM8731_power               0x00       // Disable Power down
#define _WM8731_DAIF                0x0F       // Slave mode, 32 bits, DSP con LRP=0
#define _WM8731_Sampling            0x00       // 48000Hz
#define _WM8731_Activate            0x01       // Module is ON
#define _WM8731_Deactivate          0x00       // Module is OFF
#define _WM8731_Reset               0x00       // Reset value

//Funciones
uint8_t i2c_Init(void);
uint8_t wm8731_ReadReg (uint8_t);
uint8_t wm8731_Write (uint8_t, uint8_t);
void 	wm8731_Init (void);
void 	wm8731_SetVolume (uint8_t, uint8_t);
void 	wm8731_Activate (void);
void 	wm8731_Deactivate (void);
