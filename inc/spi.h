
#include "board.h"
#include "chip.h"

#include "spi_17xx_40xx.h"

#define BUFFER_SIZE                         (0x100)

#define SPI_MASTER_MODE_SEL                 (0x31)
#define SPI_SLAVE_MODE_SEL                  (0x32)
#define SPI_MODE_SEL                        (SPI_MASTER_MODE_SEL)

#define SPI_POLLING_SEL                     (0x31)
#define SPI_INTERRUPT_SEL                   (0x32)

#define SPI_TRANSFER_MODE_SEL               (SPI_POLLING_SEL)

void SPI_Init(void);
void Read_SPI (uint8_t *, uint32_t);
void bufferInit(uint8_t *, uint8_t *);
