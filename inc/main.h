#ifndef _MAIN_H_
#define _MAIN_H_

/*==================[inclusions]=============================================*/

#include "board.h"
#include "chip.h"
#include "lcd.h"
#include "wm8731.h"
#include "spi.h"

//#include "stdint.h"
#include "string.h"

#define ON	1
#define OFF 0

#define PORT0	0
#define PORT1 	1
#define PORT2	2

typedef 	unsigned long 		uint32_t;
typedef 	unsigned short 		uint16_t;
typedef 	unsigned char 		uint8_t;

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief main function
 * @return main function should never return
 */
int main(void);

/*==================[cplusplus]==============================================*/

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _MAIN_H_ */
