/*
 * @brief SPI para el wm8731
 * En este caso usamos spi por pooling
 *
 * @note
 * Proyecto Medidas Electronicas I
 * Grupo 5	Año 2016
 *
 */

#include "main.h"

//static uint8_t spi_tx_buf[BUFFER_SIZE];
//static uint8_t spi_rx_buf[BUFFER_SIZE];
static SPI_CONFIG_FORMAT_T spi_format;
static SPI_DATA_SETUP_T spi_xf;

void SPI_Init(void){

	Board_SPI_Init(ON);
	Chip_SPI_Init(LPC_SPI);
	//Chip_SPI_SetBitRate(LPC_SPI, 400000);

	/* SPI seteo del formato */
	spi_format.bits = SPI_BITS_8;
	spi_format.clockMode = SPI_CLOCK_MODE0;  //CPHA = 0, CPOL = 0 => First clock edge; SCK active in high
	spi_format.dataOrder = SPI_DATA_MSB_FIRST;
	Chip_SPI_SetFormat(LPC_SPI, &spi_format);

	/* SPI inicializacion de la estructura de transferencia */
	spi_xf.fnBefFrame =  Board_SPI_AssertSSEL;
	spi_xf.fnAftFrame =  Board_SPI_DeassertSSEL;
	spi_xf.fnBefTransfer = NULL;
	spi_xf.fnAftTransfer = NULL;
	Chip_SPI_SetMode(LPC_SPI, SPI_MODE_MASTER);

}


void Read_SPI (uint8_t *BufferSPI_Rx, uint32_t BufferSize){

	spi_xf.cnt = 0;
	spi_xf.length = BufferSize;
	spi_xf.pTxData = NULL;
	spi_xf.pRxData = BufferSPI_Rx;

	//BufferInit(BufferSPI_Rx, NULL);

	//Envia y recibe 1 o 2 bytes segun configuracion de la estructura de formato
	Chip_SPI_RWFrames_Blocking(LPC_SPI, &spi_xf);
}
