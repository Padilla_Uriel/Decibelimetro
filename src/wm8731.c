/*
 * wm87331.c
 *
 *  Created on: 2016
 *      Author: Grupo 5
 */

#include "main.h"

uint8_t i2c_Init(void)
{
	Board_I2C_Init(I2C1);
	Chip_I2C_SetClockRate(I2C1, 100000);
	//Chip_I2C_SetClockRate(I2C1, 55555);
	//Chip_I2C_SetClockRate(I2C1, 50000);
	Chip_I2C_SetMasterEventHandler(I2C1, Chip_I2C_EventHandlerPolling);

	return 0;
}

//Comunicacion I2C con el codec, escritura de registros
uint8_t wm8731_Write(uint8_t registro, uint8_t data){

	uint8_t wbuf[2];
	I2C_XFER_T xfer;

	uint32_t i=0;

	xfer.rxBuff = 0;
	xfer.rxSz = 0;
	xfer.slaveAddr = WM8731_ADDRESS;
	xfer.status = 0;

	wbuf[0] = registro;
	wbuf[1] = data;

	xfer.txBuff = wbuf;
	xfer.txSz = 2;

	Chip_I2C_MasterTransfer(I2C1, &xfer);

	//Delay
	for(i=0; i<1000; i++);

	return 0;
}

//Lectura de registros mediante I2C
uint8_t wm8731_ReadReg(uint8_t registro){

	uint8_t wbuf;
	uint8_t rbuf;
	I2C_XFER_T xfer;
	uint32_t i = 0;

	wbuf = registro;

	xfer.rxBuff = 0;
	xfer.rxSz = 0;
	xfer.slaveAddr = WM8731_ADDRESS;
	xfer.status = 0;
	xfer.txBuff = &wbuf;
	xfer.txSz = 1;

	Chip_I2C_MasterTransfer(I2C1, &xfer);
	for(i=0; i<0xFFFF; i++);

	xfer.rxBuff = &rbuf;
	xfer.rxSz = 1;
	xfer.slaveAddr = WM8731_ADDRESS;
	xfer.status = 0;
	xfer.txBuff = 0;
	xfer.txSz = 0;

	Chip_I2C_MasterTransfer(I2C1, &xfer);
	for(i=0; i<0xFFFF; i++);

	return rbuf;
}

//Inicializacion del codec
void wm8731_Init (void){
	wm8731_Write (WM8731_REG_RESET, _WM8731_Reset);               // Reset module
	//wm8731_Write (WM8731_REG_LLINE_IN, _WM8731_left_lineIn);      // Left line in settings
	//wm8731_Write (WM8731_REG_RLINE_IN, _WM8731_Right_lineIn);     // Rigth line in settings
	//wm8731_Write (WM8731_REG_LHPHONE_OUT, _WM8731_Left_hp);       // Left headphone out settings
	//wm8731_Write (WM8731_REG_RHPHONE_OUT, _WM8731_Right_hp);      // Right headphone out settings
	wm8731_Write (WM8731_REG_ANALOG_PATH, _WM8731_AnalogAudio);   // Analog paths
	wm8731_Write (WM8731_REG_DIGITAL_PATH, _WM8731_DigitalAudio); // Digital paths
	wm8731_Write (WM8731_REG_PDOWN_CTRL, _WM8731_power);          // Power down control
	wm8731_Write (WM8731_REG_DIGITAL_IF, _WM8731_DAIF);           // Digital interface
	wm8731_Write (WM8731_REG_SAMPLING_CTRL, _WM8731_Sampling);    // Sampling control
}

//Seteo del volumen
void wm8731_SetVolume(uint8_t LeftCh, uint8_t RightCh){
	unsigned int temp;

	if (LeftCh  > 80) LeftCh = 80;
	if (RightCh > 80) RightCh = 80;

	temp = LeftCh + 0x01AF;
	wm8731_Write(WM8731_REG_LHPHONE_OUT, temp);      //left headphone out

	temp = RightCh + 0x01AF;
	wm8731_Write(WM8731_REG_RHPHONE_OUT, temp);     //right headphone out
}

//Activar wm8731
void wm8731_Activate (void){
  wm8731_Write(WM8731_REG_ACTIVE_CTRL, _WM8731_Activate);
}

//Desactivar wm8731
void wm8731_Deactivate (void){
  wm8731_Write(WM8731_REG_ACTIVE_CTRL, _WM8731_Deactivate);
}
