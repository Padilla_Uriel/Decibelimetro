/*
===============================================================================
 Nombre      : lcd.c
 Autor	     : Cátedra: Técnicas Digitales II - UTN FRH
 Versión     : 2.0
 Fecha 		 : Junio 2014
 Descripción : Contiene las funciones básicas para controlar un display HD44780
===============================================================================
*/
#include "lcd.h"


uint32_t usTicks;

/* Inicializa al display mediante instrucciones en modo 4-bit */
void LCD_Init(void)
{
	Chip_IOCON_PinMuxSet(LPC_IOCON, LCD_D4, FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LCD_D5, FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LCD_D6, FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LCD_D7, FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LCD_RS, FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LCD_E, FUNC0);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD_D4);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD_D5);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD_D6);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD_D7);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD_RS);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD_E);

	/* Se inicializa al Timer 1 */
	LCD_Tim1Init();

	Chip_GPIO_SetPinState(LPC_GPIO, LCD_RS, OFF);
	Chip_GPIO_SetPinState(LPC_GPIO, LCD_E, OFF);

	/* Se envían tres nibbles 0x03 con los delays correspondientes */
	LCD_usDelay(16000);
	LCD_SendNibble(0x03);
	LCD_usDelay(5000);
	LCD_SendNibble(0x03);
	LCD_usDelay(200);
	LCD_SendNibble(0x03);

	/* Se cambia a modo 4-bit */
	LCD_SendNibble(0x02);
	LCD_usDelay(1000);

	/* Se envían las instrucciones requeridas */
	LCD_SendInstruction(LCD_FUNCTION_SET_4BIT);
	LCD_SendInstruction(LCD_DISPLAY_OFF);
	LCD_SendInstruction(LCD_DISPLAY_CLEAR);
	LCD_SendInstruction(LCD_ENTRY_MODE_SET);
	LCD_SendInstruction(LCD_DISPLAY_ON);
}

/* Envía un nibble al display */
void LCD_SendNibble(uint8_t theNibble)
{
	/* Se coloca cada bit del nibble en el pin correspondiente */
	if (theNibble & 0x01) (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D4, ON));
	else			   	  (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D4, OFF));

	if (theNibble & 0x02) (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D5, ON));
	else			      (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D5, OFF));

	if (theNibble & 0x04) (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D6, ON));
	else			   	  (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D6, OFF));

	if (theNibble & 0x08) (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D7, ON));
	else			      (Chip_GPIO_SetPinState(LPC_GPIO, LCD_D7, OFF));

	Chip_GPIO_SetPinState(LPC_GPIO, LCD_E, ON);
	LCD_usDelay(5);
	Chip_GPIO_SetPinState(LPC_GPIO, LCD_E, OFF);
	LCD_usDelay(5);
}


/* Envía una instrucción al display */
void LCD_SendInstruction(uint8_t theInstruction)
{
	Chip_GPIO_SetPinState(LPC_GPIO, LCD_RS, OFF);

	LCD_SendByte(theInstruction);

	if (theInstruction == LCD_DISPLAY_CLEAR)
		LCD_usDelay(2000);
	else
		LCD_usDelay(50);
}

/* Envía un caracter al display */
void LCD_SendChar(char theChar)
{
	Chip_GPIO_SetPinState(LPC_GPIO, LCD_RS, ON);

	LCD_SendByte(theChar);

	LCD_usDelay(50);
}

/* Envía un byte al display */
void LCD_SendByte(uint8_t theByte)
{
	/* Primero se envía la parte alta */
	LCD_SendNibble(theByte >> 4);

	/* Luego se envía la parte baja */
	LCD_SendNibble(theByte);
}

/* Envía un string al display */
void LCD_Print(char *p)
{
	while(*p != 0){
		LCD_SendChar(*p);
		p++;
	}
}

/* Posiciona el cursor en la columna x - fila y */
void LCD_GoToxy(uint8_t x, uint8_t y)
{
	if (y == 0)
    	LCD_SendInstruction(LCD_DDRAM_ADDRESS + LCD_START_LINE1 + x);
    else if (y == 1)
    	LCD_SendInstruction(LCD_DDRAM_ADDRESS + LCD_START_LINE2 + x);
    else if (y == 2)
    	LCD_SendInstruction(LCD_DDRAM_ADDRESS + LCD_START_LINE3 + x);
    else if (y == 3)
    	LCD_SendInstruction(LCD_DDRAM_ADDRESS + LCD_START_LINE4 + x);
}

/* Delay us */
void LCD_usDelay(uint32_t usec)
{
	Chip_TIMER_SetMatch(LPC_TIMER1, 0, usec);
	Chip_TIMER_Reset(LPC_TIMER1);
	Chip_TIMER_Enable(LPC_TIMER1);

	while (!usTicks) __WFI();
	usTicks = 0;

	Chip_TIMER_Disable(LPC_TIMER1);

}


/* Inicializa al Timer 1 utilizado para realizar los delays de us */
void LCD_Tim1Init(void)
{
	Chip_TIMER_Init(LPC_TIMER1);
	Chip_TIMER_PrescaleSet(LPC_TIMER1,
		Chip_Clock_GetPeripheralClockRate(SYSCTL_PCLK_TIMER1) / 1000000 - 1);
	Chip_TIMER_MatchEnableInt(LPC_TIMER1, 0);
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER1, 0);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER1, 0);

	Chip_TIMER_SetMatch(LPC_TIMER1, 0, 1000);

	NVIC_EnableIRQ(TIMER1_IRQn);
}


/* Desactiva al Timer 1 */
void LCD_Tim1DeInit(void)
{
	Chip_TIMER_Disable(LPC_TIMER1);
	NVIC_DisableIRQ(TIMER1_IRQn);
}


/* Handler de interrupción del Timer 1 */
void TIMER1_IRQHandler(void)
{
	if (Chip_TIMER_MatchPending(LPC_TIMER1, 0)) {
		usTicks++;
	}
	Chip_TIMER_ClearMatch(LPC_TIMER1, 0);
}
